﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighLightTest : MonoBehaviour {

	[SerializeField] ButtonHighlighter highLighter;

	void Start () {
		highLighter.Highlight(SteamVR_Controller.ButtonMask.Grip);
	}
	
	void Update () {
		
	}
}
