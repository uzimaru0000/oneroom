﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour {

	Material material;

	bool flag;

	// Use this for initialization
	void Start () {
		material = GetComponent<MeshRenderer>().material;
	}
	
	public void ColorChange() {
		flag = !flag;
		material.SetColor("_Color", flag ? Color.red : Color.white);
	}
}
