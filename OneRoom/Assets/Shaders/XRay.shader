﻿Shader "Custom/XRay" {
	Properties {
		_RimColor ("RimColor", Color) = (1,1,1,1)
		_Power ("Power", Float) = 1.0
	}
	SubShader {
		Tags { "Queue"="Transparent" }
		LOD 200
		ZTest Always

		Stencil {
			Ref 1
			Comp Always
			Pass Replace
		}

		CGPROGRAM
		
		#pragma surface surf Standard fullforwardshadows alpha:fade

		#pragma target 3.0

		struct Input {
			float3 worldNormal;
			float3 viewDir;
		};

		fixed4 _RimColor;
		fixed _Power;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed rim = 1 - saturate(dot(IN.viewDir, IN.worldNormal));
			o.Emission = _RimColor * pow(rim, _Power);
			o.Alpha = rim;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
