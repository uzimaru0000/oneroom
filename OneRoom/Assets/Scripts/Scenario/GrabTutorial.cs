﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabTutorial : Scenario {

	[SerializeField] ButtonHighlighter leftHighLighter;
	[SerializeField] ButtonHighlighter rightHighLighter;

	protected override void Init() {
		leftHighLighter.Highlight(SteamVR_Controller.ButtonMask.Trigger);
		rightHighLighter.Highlight(SteamVR_Controller.ButtonMask.Trigger);
	}

}
