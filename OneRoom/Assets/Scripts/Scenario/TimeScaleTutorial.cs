﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaleTutorial : Scenario {

	[SerializeField] ButtonHighlighter rightHighLighter;

	protected override void Init() {
		rightHighLighter.Highlight(SteamVR_Controller.ButtonMask.Touchpad);
	}
}
