﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XRayTutorial : Scenario {

	[SerializeField] ButtonHighlighter leftHighLighter;
	protected override void Init() {
		leftHighLighter.Highlight(SteamVR_Controller.ButtonMask.Touchpad);
	}
}
