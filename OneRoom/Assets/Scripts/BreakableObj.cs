﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody), typeof(Collider))]
public class BreakableObj : MonoBehaviour {
	
	[SerializeField] float enduranceValue; 
	[SerializeField] GameObject destroyed;

	[SerializeField] UnityEvent onBreak = new UnityEvent();

	Rigidbody body;
	Rigidbody Body {
		get {
			if (!body) body = GetComponent<Rigidbody>();
			return body;
		}
	}

	new Collider collider;
	Collider Collider {
		get {
			if (!collider) collider = GetComponent<Collider>();
			return collider;
		}
	}

	void Update () {
		if (enduranceValue < 0) {
			Destroy(gameObject);
			if (destroyed) Instantiate(destroyed, transform.position, transform.rotation);
			onBreak.Invoke();
		}
	}

	void OnCollisionEnter(Collision other) {
		var grabObj = other.gameObject.GetComponent<GrabObj>();
		var v = other.relativeVelocity;
		if (grabObj) {
			if (!grabObj.grabingObject) return;
			var hand = grabObj.grabingObject.GetComponent<Hand>();
			v += hand ? hand.Device.velocity : Vector3.zero;
		}
		enduranceValue -= 1/2.0f * Body.mass * v.sqrMagnitude;
	}
}