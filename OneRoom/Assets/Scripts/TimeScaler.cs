﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaler : MonoBehaviour {

	[SerializeField] float scale;

	public void TimeScale() {
		Time.timeScale = scale;
	}

	public void Reset() {
		Time.timeScale = 1;
	}
}
