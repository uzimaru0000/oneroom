﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class GrabObj : MonoBehaviour {

	[SerializeField] UnityEvent onGrab = new UnityEvent();
	[SerializeField] UnityEvent onRelease = new UnityEvent();

	Rigidbody body;
	Rigidbody Body {
		get {
			if (!body) body = GetComponent<Rigidbody>();
			return body;
		}
	}

	FixedJoint connectedJoint;


	public Rigidbody grabingObject {
		get {
			if (connectedJoint) {
				return connectedJoint.GetComponent<Rigidbody>();
			} else {
				return null;
			}
		}
	}

	public void Grabing(FixedJoint joint) {
		if (connectedJoint) Release();
		transform.position = joint.transform.position;
		transform.rotation = joint.transform.rotation;
		joint.connectedBody = Body;
		connectedJoint = joint;
		onGrab.Invoke();
	}

	public void Release() {
		if (connectedJoint) {
			connectedJoint.connectedBody = null;
			connectedJoint = null;
			onRelease.Invoke();
		}
	}

}
