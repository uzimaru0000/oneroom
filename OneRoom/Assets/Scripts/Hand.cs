﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour {
	public enum HandType {
		Left,
		Right
	}

	[SerializeField] HandType handType;
	SteamVR_TrackedObject trackedObject;
	public SteamVR_Controller.Device Device {
		get {
			if (!trackedObject) trackedObject = GetComponent<SteamVR_TrackedObject>();
			return (int) trackedObject.index != -1 ? SteamVR_Controller.Input((int) trackedObject.index) : null;
		}
	}
	
	FixedJoint joint;

	void Start () {
		joint = GetComponent<FixedJoint>();
	}
	
	void Update () {
		if (joint.connectedBody && Device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger)) {
			joint.connectedBody.velocity = Device.velocity;
			joint.connectedBody.GetComponent<GrabObj>().Release();
		}

		switch(handType) {
			case HandType.Left:
				XRayEnabled();
				break;
			case HandType.Right:
				TimeScale();
				break;
		}
	}

	void XRayEnabled() {
		var depth = Camera.main.GetComponent<DepsEffect>();
		if (depth && Device != null) {
			depth.enabled = Device.GetPress(SteamVR_Controller.ButtonMask.Touchpad);
			XRay.trigger = Device.GetPress(SteamVR_Controller.ButtonMask.Touchpad);
		}
	}

	void TimeScale() {
		var scaler = GetComponent<TimeScaler>();
		if (scaler && Device != null) {
			if (Device.GetPress(SteamVR_Controller.ButtonMask.Touchpad)) scaler.TimeScale();
			else scaler.Reset();
		}
	}

	void OnTriggerStay(Collider other) {
		var grabable = other.GetComponent<GrabObj>();
		grabable = grabable ? grabable : other.transform.root.GetComponent<GrabObj>();
		if (grabable && !joint.connectedBody && Device.GetPress(SteamVR_Controller.ButtonMask.Trigger)) {
			grabable.Grabing(joint);
		}
	}
}
