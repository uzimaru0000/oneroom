﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XRay : MonoBehaviour {

	public static bool trigger = false;

	MeshRenderer renderer;

	void Start() {
		renderer = GetComponent<MeshRenderer>();
	}

	void Update () {
		renderer.enabled = trigger;
	}
}
