﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody), typeof(Collider))]
public class PushButton : MonoBehaviour {

	[SerializeField] float rate;
	[SerializeField] float maxDown;
	[SerializeField] UnityEvent onDown = new UnityEvent();
	[SerializeField] UnityEvent onPress = new UnityEvent();
	[SerializeField] UnityEvent onUp = new UnityEvent();

	Vector3 defaultPos;
	bool isDown;

	Rigidbody _body;
	Rigidbody Body {
		get {
			if (!_body) _body = GetComponent<Rigidbody>();
			return _body;
		}
	}

	Vector3 lockedPos {
		get {
			var pos = defaultPos;
			pos.y = transform.localPosition.y;
			return pos;
		}
	}

	void Awake() {
		defaultPos = transform.localPosition;
	}

	void Start () {
		
	}
	
	void Update () {
		if (!isDown && DownCheck()) {
			isDown = true;
			onDown.Invoke();
		} else if (isDown && PressCheck()) {
			onPress.Invoke();
		}
		PosReset();
	}

	void FixedUpdate() {
		Stopper();
	}

	void PosReset() {
		transform.localPosition = Vector3.Lerp(transform.localPosition, defaultPos, rate * Time.deltaTime);
		transform.localPosition = lockedPos;
		if (defaultPos.y - maxDown * 0.1 <= transform.localPosition.y) {
			isDown = false;
		}
	}

	void Stopper() {
		var maxY = defaultPos.y - maxDown;
		if (maxY > transform.localPosition.y) {
			var currentPos = transform.localPosition;
			currentPos.y = maxY;
			transform.localPosition = currentPos;
		}
		if (transform.localPosition.y > defaultPos.y) {
			transform.localPosition = defaultPos;
		}
	}

	bool DownCheck() {
		var border = maxDown * 0.5;
		return defaultPos.y - border > transform.localPosition.y;
	}

	bool PressCheck() {
		var minBorder = maxDown * 0.5;
		var maxBorder = maxDown;
		return defaultPos.y - minBorder >= transform.localPosition.y && defaultPos.y - maxBorder < transform.localPosition.y;
	}

	void OnCollisionExit(Collision other) {
		if (isDown) onUp.Invoke();
	}
}
