﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debuger : MonoBehaviour {

	[SerializeField] Hand rightHand;
	[SerializeField] Hand leftHand;
	[SerializeField] float moveSpeed;

	void Start () {
		
	}
	
	void Update () {
		PlayerMove();
		PlayerRotare();
	}

	void PlayerMove() {
		if (rightHand.Device != null) {
			var axis = rightHand.Device.GetAxis();
			var move = transform.TransformDirection(new Vector3(axis.x, 0, axis.y));
			if (move.sqrMagnitude > 0) SteamVR_Fade.Start(new Color(0, 0, 0, 0.8f), 0);
			else SteamVR_Fade.Start(new Color(0, 0, 0, 0), 0);
			transform.position += move * moveSpeed * Time.deltaTime;
		}
	}

	void PlayerRotare() {
		if (leftHand.Device != null) {
			var axis = leftHand.Device.GetAxis();
			transform.rotation *= Quaternion.Euler(Vector3.up * 15 * axis.x * Time.deltaTime);
		}
	}
}
