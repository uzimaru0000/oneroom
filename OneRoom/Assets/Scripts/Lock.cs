﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Lock : MonoBehaviour {

	[SerializeField] GameObject key;
	[SerializeField] UnityEvent onUnLock = new UnityEvent();
	
	void OnCollisionEnter(Collision other) {
		if (other.gameObject == key || other.transform.root.gameObject == key) {
			onUnLock.Invoke();
		}
	}
}
