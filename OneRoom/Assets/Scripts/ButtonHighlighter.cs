﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ButtonHighlighter : MonoBehaviour {

	[SerializeField] GameObject grip;
	[SerializeField] GameObject trigger;
	[SerializeField] GameObject pad;

	Material GripMaterial {
		get {
			return GetMaterial(grip);
		}
	}

	Material TriggerMaterial {
		get {
			return GetMaterial(trigger);
		}
	}

	Material PadMaterial {
		get {
			return GetMaterial(pad);
		}
	}

	Material GetMaterial(GameObject target) {
		var renderer = target.GetComponent<MeshRenderer>();
		if (!renderer) renderer = target.GetComponentInChildren<MeshRenderer>();
		return renderer.material;
	}

	public void Highlight(System.UInt64 button) {
		Material mat = null;
		switch(button) {
			case SteamVR_Controller.ButtonMask.Grip:
				mat = GripMaterial;
				break;
			case SteamVR_Controller.ButtonMask.Trigger:
				mat = TriggerMaterial;
				break;
			case SteamVR_Controller.ButtonMask.Touchpad:
				mat = PadMaterial;
				break;
		}

		if (mat != null) {
			mat.EnableKeyword("_EmissionColor");
			mat.DOColor(Color.yellow * 2, "_EmissionColor", 0.5f).SetLoops(-1, LoopType.Yoyo);
		}
	}
}
