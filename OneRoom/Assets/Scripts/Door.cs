﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Door : MonoBehaviour {

	[SerializeField] Vector3 move;

	public void Open() {
		transform.DOMove(move, 1.0f).SetRelative();
	}
}
