﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Moveing : MonoBehaviour {

	[SerializeField] Vector3 dir;
	[SerializeField] float speed;
	[SerializeField] bool isRelative;
	[SerializeField] bool looping;

	void Start () {
		var tweener = transform.DOMove(dir, speed);
		if (isRelative) tweener = tweener.SetRelative();
		if (looping) tweener = tweener.SetLoops(-1);
	}
}
