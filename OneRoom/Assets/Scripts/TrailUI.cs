﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailUI : MonoBehaviour {

	[SerializeField] GameObject target;
	[SerializeField] Vector3 offset;
	
	void FixedUpdate () {
		transform.position = target.transform.position + offset;
		transform.LookAt(Camera.main.transform);
	}
}
