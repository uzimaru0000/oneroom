﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[ExecuteInEditMode, RequireComponent(typeof(Camera))]
public class DepsEffect : MonoBehaviour {

	[SerializeField] Shader _shader;
	Material _material;
	Material Material {
		get {
			if (!_material) _material = new Material(_shader);
			return _material;
		}
	}
	CommandBuffer _buffer;
	CommandBuffer Buffer {
		get {
			if (_buffer == null) {
				var imageId = Shader.PropertyToID("_tempTex");
				_buffer = new CommandBuffer { name = "XRay" };
				_buffer.GetTemporaryRT(imageId, -1, -1, 0);
				_buffer.Blit(BuiltinRenderTextureType.CameraTarget, imageId);
				_buffer.Blit(imageId, BuiltinRenderTextureType.CameraTarget, Material);
			}
			return _buffer;
		}
	}
	Camera _camera;
	Camera Camera {
		get {
			if (!_camera) {
				_camera = GetComponent<Camera>();
				_camera.depthTextureMode |= DepthTextureMode.Depth;
			}
			return _camera;
		}
	}

	void OnEnable() {
		Camera.AddCommandBuffer(CameraEvent.AfterForwardAlpha, Buffer);		
	}

	void OnDisable() {
		Camera.RemoveCommandBuffer(CameraEvent.AfterForwardAlpha, Buffer);		
	}

}
