﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Rigidbody))]
public class Roller : MonoBehaviour {

	[SerializeField] Vector3 axis;
	[SerializeField] float quantity;

	Rigidbody _body;
	Rigidbody Body {
		get {
			if (!_body) _body = GetComponent<Rigidbody>();
			return _body;
		}
	}

	[SerializeField] bool flag;

	void Update() {
		if (flag) Body.MoveRotation(transform.rotation * Quaternion.Euler(axis * quantity * Time.fixedDeltaTime));
	}
	
	public void RollStart() {
		flag = true;
	}

	public void RollEnd() {
		flag = false;
	}
}
