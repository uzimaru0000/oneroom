﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	static GameManager _instance;
	public static GameManager Instance {
		get {
			return _instance;
		}
	}

	int currentSceneIndex = 0;
	public float StayTime {
		get;
		private set;
	}

	[SerializeField] List<SceneObject> sceneList = new List<SceneObject>();

	void Awake() {
		if (!_instance) {
			_instance = this;
			DontDestroyOnLoad(gameObject);
		} else {
			Destroy(gameObject);
		}
	}
	
	void Update () {
		StayTime += Time.deltaTime;
	}

	public void SceneLoad(int index) {
		SteamVR_LoadLevel.Begin(sceneList[index]);
	}

	public void NextScene(){
		currentSceneIndex = (currentSceneIndex+1) % sceneList.Count;
		SceneLoad(currentSceneIndex);
	}

	public void ResetScene(){
		SceneLoad(currentSceneIndex);
	}
}
