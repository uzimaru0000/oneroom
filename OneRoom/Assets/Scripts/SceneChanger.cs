﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneChanger : MonoBehaviour {

	public void NextScene() {
		GameManager.Instance.NextScene();
	}

	public void ResetScene() {
		GameManager.Instance.ResetScene();
	}
}
