﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Counter : MonoBehaviour {

	[SerializeField] int targetValue;
	[SerializeField] UnityEvent onEqual = new UnityEvent();

	int counter;

	public void Increment() {
		counter++;
		if (counter == targetValue) onEqual.Invoke();
	}

	public void Decrement() {
		counter--;
		if (counter == targetValue) onEqual.Invoke();
	}
}
