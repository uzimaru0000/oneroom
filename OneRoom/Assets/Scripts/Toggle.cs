﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Toggle : MonoBehaviour {

	[SerializeField] UnityEvent onToggleOn = new UnityEvent();
	[SerializeField] UnityEvent onToggleOff = new UnityEvent();

	bool toggle = false;

	public void Switch() {
		toggle = !toggle;
		if (toggle) onToggleOn.Invoke();
		if (!toggle) onToggleOff.Invoke();
	}
}
