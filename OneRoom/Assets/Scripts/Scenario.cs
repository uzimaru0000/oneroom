﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Scenario : MonoBehaviour {

	void Start () {
		Init();
	}

	protected abstract void Init();
}
