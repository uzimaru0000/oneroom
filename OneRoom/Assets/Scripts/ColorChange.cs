﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ColorChange : MonoBehaviour {

	[SerializeField, ColorUsage(false, true)]
	Color changeColor;

	[SerializeField]
	string propertyName;

	[SerializeField]
	float duration;

	[SerializeField]
	new Renderer renderer;

	public void ChangeColor() {
		var mat = renderer != null ? renderer.material : GetComponent<MeshRenderer>().material;
		mat.EnableKeyword(propertyName);
		mat.DOColor(changeColor, propertyName, duration);
	}
}
