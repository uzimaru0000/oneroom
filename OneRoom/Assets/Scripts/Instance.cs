﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instance : MonoBehaviour {

	[SerializeField] GameObject obj;
	[SerializeField] int num;

	void Start () {
		for (int i = 0; i < num; i++) {
			Instantiate(obj, transform.position + Random.insideUnitSphere, Random.rotation);
		}		
	}

}
